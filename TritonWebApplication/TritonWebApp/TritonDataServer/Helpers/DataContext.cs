﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using TritonDataServer.Entities;

namespace TritonDataServer.Helpers
{
    public class DataContext : DbContext
    {
        protected readonly IConfiguration Configuration;

        public DataContext(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            options.UseSqlServer(Configuration.GetConnectionString("TritonDatabaseConnection"));
        }

        public DbSet<Truck> Trucks { get; set; }
        public DbSet<Branch> Branchs { get; set; }
        public DbSet<WeighBill> WeighBills { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Branch>().HasData(
                new Branch { Id = 1, BranchName = "Johannesburg", Province = "Gauteng" },
                new Branch { Id = 2, BranchName = "Capetown", Province = "EasternCape" },
                new Branch { Id = 3, BranchName = "Durban", Province = "KwaZuluNatal" },
                new Branch { Id = 4, BranchName = "Isipingo", Province = "KwaZuluNatal" },
                new Branch { Id = 5, BranchName = "Polokwane", Province = "Limpopo" },
                new Branch { Id = 6, BranchName = "Dikgopo", Province = "NorthWest" }
            );

            modelBuilder.Entity<WeighBill>().HasData(
                new WeighBill() { Id = 1, CategoryName = "Category-A", MaximumWeight = 65000, MinimumWeight = 30000 },
                new WeighBill() { Id = 2, CategoryName = "Category-B", MaximumWeight = 75000, MinimumWeight = 40000 },
                new WeighBill() { Id = 3, CategoryName = "Category-C", MaximumWeight = 85000, MinimumWeight = 20000 },
                new WeighBill() { Id = 4, CategoryName = "Category-D", MaximumWeight = 95000, MinimumWeight = 10000 }
                );

           modelBuilder.Entity<Truck>().HasData(
                new Truck { Id = 1, ModelName = "XFactor", RegistrationNumber = "ND-6575709", NetWeight = 25000, TareWeight = 55000, BranchId = 1, WeighBillId = 1 },
                new Truck { Id = 2, ModelName = "Iron-Man", RegistrationNumber = "YY -657-GP", NetWeight = 30000, TareWeight = 60000, BranchId = 2 , WeighBillId = 2},
                new Truck { Id = 3, ModelName = "Ghost", RegistrationNumber = "YY -6887-L", NetWeight = 30000, TareWeight = 60000, BranchId = 2, WeighBillId = 2 },
                new Truck { Id = 4, ModelName = "Spider-Man", RegistrationNumber = "ND-6575709", NetWeight = 25000, TareWeight = 55000, BranchId = 3, WeighBillId = 1 }
                );
        }
    }
}
