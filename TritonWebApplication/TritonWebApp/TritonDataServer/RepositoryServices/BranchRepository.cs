﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TritonDataServer.DataAccessTypes;
using TritonDataServer.Entities;
using TritonDataServer.Helpers;

namespace TritonDataServer.RepositoryServices
{
    public class BranchRepository : IBranchRepository
    {
        private DataContext _context;

        public BranchRepository(DataContext context)
        {
            _context = context;
        }
        public IQueryable<Branch> GetAll()
        {
            return _context.Branchs;
        }

        public async Task<Branch> GetById(int id)
        {
            var branchModel =  _context.Branchs.Find(id);
            return branchModel;
        }

        public Branch Create(Branch branchDataModel)
        {
            _context.Branchs.Add(branchDataModel);
            _context.SaveChanges();

            return branchDataModel;
        }

        public void Update(Branch branchDataModel)
        {
            var branchToUpdate = _context.Branchs.Find(branchDataModel.Id);

            if (!string.IsNullOrWhiteSpace(branchDataModel.BranchName))
                branchToUpdate.BranchName = branchDataModel.BranchName;

            if (!string.IsNullOrWhiteSpace(branchDataModel.Province))
                branchToUpdate.Province = branchDataModel.Province;


            _context.Branchs.Update(branchToUpdate);
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            var branch = _context.Branchs.Find(id);
            if (branch != null)
            {
                _context.Branchs.Remove(branch);
                _context.SaveChanges();
            }
        }
    }
}
