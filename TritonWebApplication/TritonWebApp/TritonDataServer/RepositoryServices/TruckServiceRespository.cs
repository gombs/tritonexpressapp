﻿using System.Collections.Generic;
using System.Linq;
using TritonDataServer.DataAccessTypes;
using TritonDataServer.Entities;
using TritonDataServer.Helpers;

namespace TritonDataServer.RepositoryServices
{
    public class TruckServiceRespository: ITruckServiceRepository
    {
        private DataContext _context;

        public TruckServiceRespository(DataContext context)
        {
            _context = context;
        }

        public IQueryable<Truck> GetAll()
        {
            return _context.Trucks;
        }

        public Truck GetById(int id)
        {
            return _context.Trucks.Find(id);
        }

        public Truck Create(Truck truck)
        {
            _context.Trucks.Add(truck);
            _context.SaveChanges();

            return truck;
        }

        public void Update(Truck truck)
        {
            var truckToUpdate = _context.Trucks.Find(truck.Id);

            if (!string.IsNullOrWhiteSpace(truck.ModelName))
                truckToUpdate.ModelName = truck.ModelName;

            if (!string.IsNullOrWhiteSpace(truck.RegistrationNumber))
                truckToUpdate.RegistrationNumber = truck.RegistrationNumber;

            if (!string.IsNullOrWhiteSpace(truck.NetWeight.ToString()))
                truckToUpdate.NetWeight = truck.NetWeight;

            if (!string.IsNullOrWhiteSpace(truck.TareWeight.ToString()))
                truckToUpdate.TareWeight = truck.TareWeight;

            if (!(truck.WeighBill == null))
                truckToUpdate.WeighBill = truck.WeighBill;


            _context.Trucks.Update(truckToUpdate);
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            var truck = _context.Trucks.Find(id);
            if (truck != null)
            {
                _context.Trucks.Remove(truck);
                _context.SaveChanges();
            }
        }
    }
}
