﻿using System.Collections.Generic;
using System.Linq;
using TritonDataServer.DataAccessTypes;
using TritonDataServer.Entities;
using TritonDataServer.Helpers;

namespace TritonDataServer.RepositoryServices
{
    public class WeighBillServiceRepository : IWeighBillServiceRepository
    {
        private DataContext _context;

        public WeighBillServiceRepository(DataContext context)
        {
            _context = context;
        }

        public IQueryable<WeighBill> GetAll()
        {
            return _context.WeighBills;
        }

        public WeighBill GetById(int id)
        {
            return _context.WeighBills.Find(id);
        }

        public WeighBill Create(WeighBill weighBillDataModel)
        {
            _context.WeighBills.Add(weighBillDataModel);
            _context.SaveChanges();

            return weighBillDataModel;
        }

        public void Update(WeighBill weighBillDataModel)
        {
            var weighBillDataModelToUpdate = _context.WeighBills.Find(weighBillDataModel.Id);

            if (!string.IsNullOrWhiteSpace(weighBillDataModel.CategoryName))
                weighBillDataModelToUpdate.CategoryName = weighBillDataModel.CategoryName;

            if (weighBillDataModel.MaximumWeight != 0)
                weighBillDataModelToUpdate.MaximumWeight = weighBillDataModel.MaximumWeight;

            if (weighBillDataModel.MinimumWeight != 0)
                weighBillDataModelToUpdate.MinimumWeight = weighBillDataModel.MinimumWeight;
            
            _context.WeighBills.Update(weighBillDataModelToUpdate);
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            var weighBill = _context.WeighBills.Find(id);
            if (weighBill != null)
            {
                _context.WeighBills.Remove(weighBill);
                _context.SaveChanges();
            }
        }
    }
}
