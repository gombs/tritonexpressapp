﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace TritonDataServer.Entities
{
    public class WeighBill
    {
        public int Id { get; set; }
        public string CategoryName { get; set; }
        public int MaximumWeight { get; set; }
        public int MinimumWeight { get; set; }
        public ICollection<Truck> Trucks { get; set; }
    }
}
