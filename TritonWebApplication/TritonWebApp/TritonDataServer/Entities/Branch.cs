﻿using System.Collections.Generic;
using TritonDataServer.DataAccessTypes;

namespace TritonDataServer.Entities
{
    public class Branch : IDataModel
    {
        public int Id { get; set; }
        public string Province { get; set; }
        public string BranchName { get; set; }
        public ICollection<Truck> Trucks { get; set; }
    }
}
