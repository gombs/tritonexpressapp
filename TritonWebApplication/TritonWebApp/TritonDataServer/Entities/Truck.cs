﻿using TritonDataServer.DataAccessTypes;

namespace TritonDataServer.Entities
{
    public class Truck : IDataModel
    {
        public int Id { get; set; }
        public string ModelName { get; set; }
        public string RegistrationNumber { get; set; }
        public int TareWeight { get; set; }
        public int NetWeight { get; set; }
        public int BranchId { get; set; }
        public Branch Branch { get; set; }
        public int? WeighBillId { get; set; }
        public WeighBill WeighBill { get; set; }
    }
}
