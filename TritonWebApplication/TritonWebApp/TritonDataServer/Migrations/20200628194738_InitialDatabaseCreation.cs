﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TritonDataServer.Migrations
{
    public partial class InitialDatabaseCreation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Branchs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Province = table.Column<string>(nullable: true),
                    BranchName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Branchs", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "WeighBills",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CategoryName = table.Column<string>(nullable: true),
                    MaximumWeight = table.Column<int>(nullable: false),
                    MinimumWeight = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WeighBills", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Trucks",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ModelName = table.Column<string>(nullable: true),
                    RegistrationNumber = table.Column<string>(nullable: true),
                    TareWeight = table.Column<int>(nullable: false),
                    NetWeight = table.Column<int>(nullable: false),
                    BranchId = table.Column<int>(nullable: false),
                    WeighBillId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Trucks", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Trucks_Branchs_BranchId",
                        column: x => x.BranchId,
                        principalTable: "Branchs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Trucks_WeighBills_WeighBillId",
                        column: x => x.WeighBillId,
                        principalTable: "WeighBills",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "Branchs",
                columns: new[] { "Id", "BranchName", "Province" },
                values: new object[,]
                {
                    { 1, "Johannesburg", "Gauteng" },
                    { 2, "Capetown", "EasternCape" },
                    { 3, "Durban", "KwaZuluNatal" },
                    { 4, "Isipingo", "KwaZuluNatal" },
                    { 5, "Polokwane", "Limpopo" },
                    { 6, "Dikgopo", "NorthWest" }
                });

            migrationBuilder.InsertData(
                table: "WeighBills",
                columns: new[] { "Id", "CategoryName", "MaximumWeight", "MinimumWeight" },
                values: new object[,]
                {
                    { 1, "Category-A", 65000, 30000 },
                    { 2, "Category-B", 75000, 40000 },
                    { 3, "Category-C", 85000, 20000 },
                    { 4, "Category-D", 95000, 10000 }
                });

            migrationBuilder.InsertData(
                table: "Trucks",
                columns: new[] { "Id", "BranchId", "ModelName", "NetWeight", "RegistrationNumber", "TareWeight", "WeighBillId" },
                values: new object[,]
                {
                    { 1, 1, "XFactor", 25000, "ND-6575709", 55000, 1 },
                    { 4, 3, "Spider-Man", 25000, "ND-6575709", 55000, 1 },
                    { 2, 2, "Iron-Man", 30000, "YY -657-GP", 60000, 2 },
                    { 3, 2, "Ghost", 30000, "YY -6887-L", 60000, 2 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Trucks_BranchId",
                table: "Trucks",
                column: "BranchId");

            migrationBuilder.CreateIndex(
                name: "IX_Trucks_WeighBillId",
                table: "Trucks",
                column: "WeighBillId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Trucks");

            migrationBuilder.DropTable(
                name: "Branchs");

            migrationBuilder.DropTable(
                name: "WeighBills");
        }
    }
}
