﻿using System.Linq;
using System.Threading.Tasks;
using TritonDataServer.Entities;

namespace TritonDataServer.DataAccessTypes
{
    public interface IBranchRepository
    {
        IQueryable<Branch> GetAll();
        Task<Branch> GetById(int id);
        Branch Create(Branch dataModel);
        void Update(Branch dataModel);
        void Delete(int id);
    }
}
