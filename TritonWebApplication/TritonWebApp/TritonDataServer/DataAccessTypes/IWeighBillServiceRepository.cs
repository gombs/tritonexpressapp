﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TritonDataServer.Entities;

namespace TritonDataServer.DataAccessTypes
{
    public interface IWeighBillServiceRepository
    {
        IQueryable<WeighBill> GetAll();
        WeighBill GetById(int id);
        WeighBill Create(WeighBill dataModel);
        void Update(WeighBill dataModel);
        void Delete(int id);
    }
}
