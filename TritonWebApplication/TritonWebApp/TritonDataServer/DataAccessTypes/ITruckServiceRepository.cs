﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TritonDataServer.Entities;

namespace TritonDataServer.DataAccessTypes
{
    public interface ITruckServiceRepository
    {
        IQueryable<Truck> GetAll();
        Truck GetById(int id);
        Truck Create(Truck dataModel);
        void Update(Truck dataModel);
        void Delete(int id);
    }
}
