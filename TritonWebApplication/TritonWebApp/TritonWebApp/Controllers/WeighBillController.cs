﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using TritonDataServer.DataAccessTypes;
using TritonDataServer.Entities;
using TritonDataServer.Helpers;

namespace TritonWebApp.Controllers
{
    public class WeighBillController : Controller
    {
        private readonly DataContext _context;
        private readonly IWeighBillServiceRepository _weighBillServiceRepository;

        public WeighBillController(DataContext context, IWeighBillServiceRepository weighBillServiceRepository)
        {
            _context = context;
            _weighBillServiceRepository = weighBillServiceRepository;
        }

        // GET: WeighBill
        public async Task<IActionResult> Index()
        {
            return View(await _weighBillServiceRepository.GetAll().ToListAsync());
        }

        // GET: WeighBill/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var weighBill = await _context.WeighBills
                .FirstOrDefaultAsync(m => m.Id == id);
            if (weighBill == null)
            {
                return NotFound();
            }

            return View(weighBill);
        }

        // GET: WeighBill/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: WeighBill/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,CategoryName,MaximumWeight,MinimumWeight")] WeighBill weighBill)
        {
            if (ModelState.IsValid)
            {
                _context.Add(weighBill);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(weighBill);
        }

        // GET: WeighBill/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var weighBill = await _context.WeighBills.FindAsync(id);
            if (weighBill == null)
            {
                return NotFound();
            }
            return View(weighBill);
        }

        // POST: WeighBill/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,CategoryName,MaximumWeight,MinimumWeight")] WeighBill weighBill)
        {
            if (id != weighBill.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(weighBill);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!WeighBillExists(weighBill.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(weighBill);
        }

        // GET: WeighBill/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var weighBill = await _context.WeighBills
                .FirstOrDefaultAsync(m => m.Id == id);
            if (weighBill == null)
            {
                return NotFound();
            }

            return View(weighBill);
        }

        // POST: WeighBill/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var weighBill = await _context.WeighBills.FindAsync(id);
            _context.WeighBills.Remove(weighBill);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool WeighBillExists(int id)
        {
            return _context.WeighBills.Any(e => e.Id == id);
        }
    }
}
