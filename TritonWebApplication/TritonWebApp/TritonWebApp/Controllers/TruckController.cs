﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using TritonDataServer.DataAccessTypes;
using TritonDataServer.Entities;
using TritonDataServer.Helpers;

namespace TritonWebApp.Controllers
{
    public class TruckController : Controller
    {
        private readonly DataContext _context;
        private readonly ITruckServiceRepository _truckServiceRepository;

        public TruckController(DataContext context, ITruckServiceRepository truckServiceRepository)
        {
            _context = context;
            _truckServiceRepository = truckServiceRepository;
        }

        // GET: Truck
        public async Task<IActionResult> Index()
        {
            var dataContext = _truckServiceRepository.GetAll().Include(t => t.Branch).Include(t => t.WeighBill);
            return View(await dataContext.ToListAsync());
        }

        // GET: Truck/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var truck = await _truckServiceRepository.GetAll()
                .Include(t => t.Branch)
                .Include(t => t.WeighBill)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (truck == null)
            {
                return NotFound();
            }

            return View(truck);
        }

        // GET: Truck/Create
        public IActionResult Create()
        {
            ViewData["BranchId"] = new SelectList(_context.Branchs, "Id", "Id");
            ViewData["WeighBillId"] = new SelectList(_context.WeighBills, "Id", "Id");
            return View();
        }

        // POST: Truck/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,ModelName,RegistrationNumber,TareWeight,NetWeight,BranchId,WeighBillId")] Truck truck)
        {
            if (ModelState.IsValid)
            {
                _truckServiceRepository.Create(truck);
                return RedirectToAction(nameof(Index));
            }
            ViewData["BranchId"] = new SelectList(_context.Branchs, "Id", "Id", truck.Branch.BranchName);
            ViewData["WeighBillId"] = new SelectList(_context.WeighBills, "Id", "Id", truck.WeighBillId);
            return View(truck);
        }

        // GET: Truck/Edit/5
        public async Task<IActionResult> Edit(int id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var truck =  _truckServiceRepository.GetById(id);
            if (truck == null)
            {
                return NotFound();
            }
            ViewData["BranchId"] = new SelectList(_context.Branchs, "Id", "Id", truck.BranchId);
            ViewData["WeighBillId"] = new SelectList(_context.WeighBills, "Id", "Id", truck.WeighBillId);
            return View(truck);
        }

        // POST: Truck/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,ModelName,RegistrationNumber,TareWeight,NetWeight,BranchId,WeighBillId")] Truck truck)
        {
            if (id != truck.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _truckServiceRepository.Update(truck);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TruckExists(truck.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["BranchId"] = new SelectList(_context.Branchs, "Id", "Id", truck.BranchId);
            ViewData["WeighBillId"] = new SelectList(_context.WeighBills, "Id", "Id", truck.WeighBillId);
            return View(truck);
        }

        // GET: Truck/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var truck = await _context.Trucks
                .Include(t => t.Branch)
                .Include(t => t.WeighBill)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (truck == null)
            {
                return NotFound();
            }

            return View(truck);
        }

        // POST: Truck/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var truck = await _context.Trucks.FindAsync(id);
            _context.Trucks.Remove(truck);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TruckExists(int id)
        {
            return _context.Trucks.Any(e => e.Id == id);
        }
    }
}
