﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using TritonDataServer.DataAccessTypes;
using TritonDataServer.Entities;
using TritonWebApp.Models;

namespace TritonWebApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IBranchRepository _branchRepository;
        private readonly ITruckServiceRepository _truckServiceRepository;
        private readonly IWeighBillServiceRepository _weighBillServiceRepository;

        public HomeController(IBranchRepository branchRepository, ITruckServiceRepository truckServiceRepository, IWeighBillServiceRepository weighBillServiceRepository)
        {
            _branchRepository = branchRepository;
            _truckServiceRepository = truckServiceRepository;
            _weighBillServiceRepository = weighBillServiceRepository;
        }

        public IActionResult Index()
        {
            var allBranches = _branchRepository.GetAll().ToList();

            foreach (var branch in allBranches)
            {
                var listOfTrucks = new List<Truck>();
                listOfTrucks = _truckServiceRepository.GetAll().Where(x => x.BranchId == branch.Id).ToList();

                foreach (var truck in listOfTrucks)
                {
                    var weighBill = new WeighBill();
                    weighBill = _weighBillServiceRepository.GetById((int) truck.WeighBillId);
                    truck.WeighBill = weighBill;
                }

                branch.Trucks = listOfTrucks;
            }

            return View(allBranches);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
