TRITON EXPRESS APPLICATION

To view the actual source code for this application developed in dotnet core 3.1

Clone the repo using git.

With SSH: git@gitlab.com:gombs/tritonexpressapp.git

With Https: https://gitlab.com/gombs/tritonexpressapp.git


Application uses:

* Dot net Core 3.1
* MS-SQL database for persistence
* Entity Framework Core 6 (ORM)

Upon cloning, update database connection details thus:

- Update `AppSettings.json` file in [TritonDatabaseConnection] : Add your local database credentials (Username and password). Otherwise if you have the databse running locally.

application with use your local machine details.


Run : Data-Migrations while pointing at "TritonDataServer"   

This will seed the application sample data. That helps kick start the database with sample data.




